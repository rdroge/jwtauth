# JWTAuth

For this project you'll need to have a running instance of KeyCloak (https://www.keycloak.org).
You can run this as a Docker container as well (https://hub.docker.com/r/jboss/keycloak/).

Once you have the instance running, you can import the realm-export.json file into KeyCloak to setup the security realm.

Also in the oeablSecurity.properties you'll need to adjust the end point to reflect the end point of your KeyCloak instance:

jwtToken.keystore.jwkurl

Look into the domains.json file for the setup of the OpenEdge database security domain.

The oeablSecurityJWT.csv contains the role based URL access.

You can have a look at the other repository (https://gitlab.com/rdroge/loginpage) where I built a login page that uses the KeyCloak 
client-side JavaScript library to actually authenticate against the Keycloak realm.

#Future work

- Upgrade the project to OpenEdge 12.2
- Create a Docker image based on this
- Further instructions on setting up the project



  
